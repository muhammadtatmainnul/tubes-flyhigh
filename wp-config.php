<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'FLYHIGH' );

/** MySQL database username */
define( 'DB_USER', 'flyhighuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'flyhighpass' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$@QE]wPt%+Yn^[$(P>Fp(@g3!(v5h!6b?*Q.+kdd})[Gep!vvbV*l#+Q|G0o,KTt' );
define( 'SECURE_AUTH_KEY',  '.,}/$UR>V[[6X1O!Y,@}0e~Cs]}7|eG@%P(EX_A>R(1x7l<..Wl_8^3$~f pIg5N' );
define( 'LOGGED_IN_KEY',    'mil~ulZl`-7/ZORR~.wQP.@;Htj@&H22/h$Mm)sI.&2.8^_W+8&y+t0<]s,Wc@?z' );
define( 'NONCE_KEY',        'g.+m{>tOgJ:sjwP-c!;!;k3b7?>QGY(^ik;tAD%I+FqytO%0!];I)L$tf }]Gzti' );
define( 'AUTH_SALT',        '`quQ: 2:)GBtS;i&gT0^Z!{ygp=c{#AEPcv!9Fe{a^2s5#1d9~U%{y6!A64zF7tU' );
define( 'SECURE_AUTH_SALT', '0%tXkIKg2G|cda*&J^[s E+2#+RO8V61J^?/3nP0^V2m?~8wEP2Anvy#W`?LWF.1' );
define( 'LOGGED_IN_SALT',   'H4>1R%;($1)^r4dq$J5&`wtN-XdGTk.MEp^O`b@3nD!OaVnlM1&6n(B/Z?*-5U=(' );
define( 'NONCE_SALT',       'eM?aD=@)dMRn{-;8[^^w=Qpx6Kt)S5ti`:kegSY2TwdS*Fi>D*fR|<lOoBC`;hCI' );

#ditambahin tatma
define('FS_METHOD'.'direct');
/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

